# CPP Lesson task: Drawin a square

This is the first c++ coding task for the CPP Embedded Development Bootcamp. Program takes in user input and print a square according to said input.

# Building

	$ mkdir build
	$ g++ square.cpp -o ./build/square

# Usage

Run the program with:

	$ ./build/square

![square001](./imgs/square001.png)

Created by Elmo Rohula
