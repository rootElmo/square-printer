#include <iostream>

int main() {
	int user_input;
	std::cout << "Enter a number for columns and rows of a square" << std::endl;;

	do {
		/*Check user input value for incorrect types*/
		if (!(std::cin >> user_input)) {
			std::cout << "Not a number!" << std::endl;
			std::cin.clear();
			std::cin.ignore(10000,'\n');

			user_input = -1;
		} else if (user_input < 0) {
			std::cout << "Number cannot be negative!" << std::endl;

			user_input = -1;
		}
	std::cout << "Please enter a number" << std::endl;
	} while (user_input == -1);

	for (int i = 0; i < user_input; i++) {
		for (int ii = 0; ii < user_input; ii++) {
			if (ii == 0 || ii == user_input - 1 || i == 0 || i == user_input - 1) {

				std::cout << "*";

			} else {

				std::cout << " ";

			}
		}
		std::cout << std::endl;
	}
	return 0;
}
